
public class TriangleRectangle extends Triangle {
	
	protected double hauteur;

	TriangleRectangle(Point p, double alpha, double l, double hauteur) {
		super(p, alpha, l, Math.atan(hauteur/l), Math.PI / 2.0); //calcul de l'angle A par trigonométrie dans un triangle rectangle
		this.hauteur = hauteur;
	}
	
	TriangleRectangle(double x, double y, double alpha, double l, double hauteur) {
		this(new Point(x, y), alpha, l, hauteur);
	}
	
	public String toString() {
		return super.toString() + "hauteur=" + FormeAngulaire.round(this.hauteur) + " ; ";
	}
	
}
