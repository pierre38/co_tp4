
public class TriangleIsocele extends Triangle  {

	TriangleIsocele(Point p, double alpha, double l, double angle) {
		super(p, alpha, l, angle, angle);
	}
	
	TriangleIsocele(double x, double y, double alpha, double l, double angle) {
		this(new Point(x, y), alpha, l, angle);
	}
	
	public String toString() {
		return super.toString();
	}
	
}
