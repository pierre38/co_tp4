
public abstract class FormeAngulaire extends Forme{

	protected double alpha;
	protected double l;
	protected double angleA;
	
	FormeAngulaire(Point p, double alpha, double angleA, double l) {
		super(p);
		this.alpha = alpha;
		this.angleA = angleA;
		this.l = l;
	}

	FormeAngulaire(double x, double y, double alpha, double angleA, double l) {
		this(new Point(x, y), alpha, angleA, l);
	}

	//m�thode abstraite
	abstract Point[] points();
	
	public static double longueur(Point a, Point b) {
		return Math.sqrt(Math.pow(a.getX() - b.getX(), 2.0) + Math.pow(a.getY() - b.getY(), 2.0));
	}
	
	/**Renvoie l'arrondi de a*/
	public static double round(double a) {
		if (Math.abs(a - (double)((int)a)) < 0.5) {
			return ((double)(int)(100.0*a))/100.0;
		} else {
			return ((double)(int)(100.0*(a+1)))/100.0;
		}
	}
	
	public double getRotation() {
		return this.alpha;
	}

	
}
