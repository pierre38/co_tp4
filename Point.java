
public class Point {

	private double x, y;
	
	Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	
	/*public double distance(Point p) {
		return Math.sqrt( Math.pow(p.getX()-this.getX(), 2.0) + Math.pow(p.getY()-this.getY(), 2.0) );
	}*/
	
	public String toString() {
		return "(" + FormeAngulaire.round(x) + ',' + FormeAngulaire.round(y) + ")";
	}

	
	//getters et setters
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
}
