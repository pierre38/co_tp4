
public class Rectangle extends Parallelogramme {

	Rectangle(Point p, double alpha, double l, double ll) {
		super(p, alpha, l, ll, Math.PI / 2.0);
	}

	Rectangle(double x, double y, double alpha, double l, double ll) {
		this(new Point(x, y), alpha, l, ll);
	}
	
	public String toString() {
		return super.toString();
	}
	
}
