
public class Parallelogramme extends FormeAngulaire {

	double ll; // 2e longueur;
	
	Parallelogramme(Point p, double alpha, double l, double ll, double angleA) {
		super(p, alpha, angleA, l);
		this.ll = ll;
	}

	Parallelogramme(double x, double y, double alpha, double l, double ll, double angleA) {
		this(new Point(x, y), alpha, l, ll, angleA);
	}
	
	public boolean estRectangle() {
		return this.angleA == Math.PI / 2.0;
	}
	
	public boolean estLosange() {
		return this.l == this.ll;
	}
	
	public boolean estCarre() {
		return this.estRectangle() && this.estLosange();
	}
	
	Point[] points() {
		//coordonn�es du point p (origine)
		double ox = this.p.getX();
		double oy = this.p.getY();
		//calcul des coordonn�es des points par trigonom�trie
		Point p2 = new Point(ox + this.ll * Math.cos(this.alpha + this.angleA), oy + this.ll * Math.sin(this.alpha + this.angleA));
		Point p3 = new Point(p2.getX() + this.l * Math.cos(this.alpha), p2.getY() + this.l * Math.sin(this.alpha));
		Point p4 = new Point(ox + this.l * Math.cos(this.alpha), oy + this.l * Math.sin(this.alpha));
		Point[] points = {this.p, p2, p3, p4};
		return points;
	}
	
	public double aire() {
		double hauteur = this.ll * Math.sin(this.angleA);
		return hauteur * this.l;
	}
	
	// Pour simplifier le code et �viter les problemes d'arrondi, on impl�mente la m�thode p�rimetre
	// dans les classes Parallelogramme et Triangle plutot que dans la classe FormeAngulaire en utilisant
	// la liste des points et le calcul de distance entre les points
	public double perimetre() {
		return 2.0 * (this.l + this.ll);
	}

	public String toString() {
		return this.p.toString() + " ; alpha=" + FormeAngulaire.round(this.alpha) + " ; l=" + FormeAngulaire.round(this.l) + " ; ll=" + FormeAngulaire.round(this.ll) + " ; angleA=" + this.angleA + " ; ";
	}

}
