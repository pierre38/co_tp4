//Utile pour l'utilisation du th�or�me de Pythagore dans la m�thode estTriRect
//import java.util.Arrays;


public class Triangle extends FormeAngulaire{

	protected double angleB;
	
	Triangle(Point p, double alpha, double l, double angleA, double angleB) {
		super(p, alpha, angleA, l);
		this.angleB = angleB;
	}
	
	Triangle(double x, double y, double alpha, double l, double angleA, double angleB) {
		this(new Point(x, y), alpha, l, angleA, angleB);
	}
	
	/**Renvoie un tableau contenant les points du triangle tel que la distance entre le 1er et le 2e Point soit l*/
	Point[] points() {
		//coordonn�es du point p (origine)
		double ox = this.p.getX();
		double oy = this.p.getY();
		//calcul des coordonn�es des points par trigonom�trie
		Point p2 = new Point(ox + this.l * Math.cos(this.alpha), oy + this.l * Math.sin(this.alpha));
		double adjA = (this.l * Math.sin(this.angleA)) / Math.sin(this.angleA + this.angleB); //calcul de la longueur adjacente � angleA
		Point p3 = new Point(ox + adjA * Math.cos(this.alpha + this.angleA), oy + adjA * Math.sin(this.alpha + this.angleA));
		Point[] points = {this.p, p2, p3};
		return points;
	}
	
	public double[] angles() {
		double[] angles = new double[3];
		angles[0] = this.angleA;
		angles[1] = this.angleB;
		angles[2] = 180 - this.angleA - this.angleB;
		return angles;
	}
	
	public double aire() {
		Point[] points = this.points();
		double a = this.l;
		double b = FormeAngulaire.longueur(points[1], points[2]);
		double c = FormeAngulaire.longueur(points[2], points[0]);
		double s = (a + b + c) / 2.0;
		return Math.sqrt(s * (s - a) * (s - b) * (s - c));
	}
	
	// Pour simplifier le code et �viter les problemes d'arrondi, on impl�mente la m�thode p�rimetre
	// dans les classes Parallelogramme et Triangle plutot que dans la classe FormeAngulaire en utilisant
	// la liste des points et le calcul de distance entre les points
	public double perimetre() {
		Point[] points = this.points();
		double a = this.l;
		double b = FormeAngulaire.longueur(points[1], points[2]);
		double c = FormeAngulaire.longueur(points[2], points[0]);
		return a + b + c;
	}
	
	public boolean estTriRect() {
		double[] angles = this.angles();
		return angles[0] == Math.PI / 2.0 || angles[1] == Math.PI / 2.0 || angles[2] == Math.PI / 2.0;
		
		//On aurait pu utiliser le th�or�me de Pythagore mais dans certains cas, des probl�mes d'arrondi donnent des r�ponses fausses
		/*Point[] points = this.points();
		double[] longueurs = { this.l, FormeAngulaire.longueur(points[1], points[2]), FormeAngulaire.longueur(points[1], points[0]) }; //tableau contenant les longueurs des 3 cot�s du triangle
		Arrays.sort(longueurs); //tri du tableau dans l'ordre croissant
		return Math.pow(longueurs[0], 2.0) + Math.pow(longueurs[1], 2.0) == Math.pow(longueurs[2], 2.0);*/
	}
	
	public boolean estEquilateral() {
		double[] angles = this.angles();
		return angles[0] == 60.0 && angles[1] == 60.0; //inutile de v�rifier le dernier angle qui sera obligatoirement � 60� si les 2 autres le sont.
	}
	
	public boolean estIsocele() {
		double[] angles = this.angles();
		return angles[0] == angles[1] || angles[1] == angles[2] || angles[2] == angles[0]; 
	}
	
	
	
	public String toString() {
		return "point:" + this.p.toString() + " ; alpha=" + FormeAngulaire.round(this.getRotation()) + " ; l=" + FormeAngulaire.round(this.l) + " ; A=" + FormeAngulaire.round(this.angleA) + " ; B=" + FormeAngulaire.round(this.angleB) + " ; ";
	}

	
}
