
public class Losange extends Parallelogramme{

	Losange(Point p, double alpha, double l, double angleA) {
		super(p, alpha, l, l, angleA);
	}

	Losange(double x, double y, double alpha, double l, double angleA) {
		this(new Point(x, y), alpha, l, angleA);
	}
	
	public String toString() {
		return super.toString();
	}
	
}
