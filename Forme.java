
public abstract class Forme {

	protected Point p;
	
	Forme(Point p) {
		this.p = p;
	}
	
	Forme(double x, double y) {
		this(new Point(x, y));
	}
	
	//m�thodes abstraites
	/**Renvoie la valeur de l'aire de la forme courante : le calcul de l'aire d�pend du type de la Forme*/
	public abstract double aire();
	/**Renvoie la valeur du p�rim�tre de la forme courante : le calcul de l'aire d�pend du type de la Forme*/
	public abstract double perimetre();
	/**Renvoie une repr�sentation en chaine de caract�res de la Forme courante*/
	public abstract String toString();
	
}
