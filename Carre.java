
public class Carre extends Parallelogramme {

	Carre(Point p, double alpha, double l) {
		super(p, alpha, l, l, 90.0);
	}

	Carre(double x, double y, double alpha, double l) {
		this(new Point(x, y), alpha, l);
	}
	
	public String toString() {
		return super.toString();
	}
	
}
