
public class Test {

	public static void main(String[] args) {
		
		Point[] points;
		double[] angles;
		System.out.println("ATTENTION : lors des affichages, les valeurs sont arrondies au centième (sauf pour l'aire, le périmètre et l'angle de rotation\n");
		System.out.println("---Test de la classe Disque---");
		System.out.println("new Disque(8.1, 3.3, 5.2)");
		Disque d1 = new Disque(8.1, 3.3, 5.2);
		System.out.println("Affichage de d1 : " + d1.toString());
		Point pt1 = new Point(4.9, 8);
		Disque d2 = new Disque(pt1, 4);
		System.out.println("Affichage de d2 : " + d2.toString());;
		System.out.println("p1 : " + pt1.toString());
		System.out.println("Aire de d1 : \t\t" + d1.aire());
		System.out.println("Périmètre de d1 : \t" + d1.perimetre());
		System.out.println("Diamètre de d1 : \t" + d1.diametre());
		
		System.out.println("\n---Test de la classe Triangle---");
		System.out.println("new Triangle(3, 3.4, 4, 2, 4.6, 8)");
		Triangle t1 = new Triangle(3, 3.4, 4, 2, 4.6, 8);
		System.out.println("Affichage de t1 : " + t1.toString());
		points = t1.points();
		System.out.println("Liste des points du triangle :");
		for (int i = 0; i < 3; i++) System.out.println("Point " + (i+1) + " : " + points[i].toString());
		angles = t1.angles();
		System.out.println("Liste des angles du triangle :");
		for (int i = 0; i < 3; i++) System.out.println("Angle " + (i+1) + " : " + angles[i]);
		System.out.println("Aire de t1 : \t\t" + t1.aire());
		System.out.println("Périmètre de t1 : \t" + t1.perimetre());
		System.out.println("Angle de rotation de la figure : " + t1.getRotation());
		System.out.println("Le triangle est isocèle: " + t1.estIsocele());
		System.out.println("Le triangle est rectangle : " + t1.estTriRect());
		System.out.println("Le triangle est equilatéral : " + t1.estEquilateral());
		
		System.out.println("\n---Test de la classe Parallélogramme---");
		System.out.println("new Parallelogramme(3, 3.4, 4, 2, 4.6, 8)");
		Parallelogramme p1 = new Parallelogramme(3, 3.4, 4, 2, 4.6, 8);
		System.out.println("Affichage de p1 : " + p1.toString());
		points = p1.points();
		System.out.println("Liste des points du parallélogramme :");
		for (int i = 0; i < 4; i++) System.out.println("Point " + (i+1) + " : " + points[i].toString());
		System.out.println("Aire de t1 : \t\t" + p1.aire());
		System.out.println("Périmètre de t1 : \t" + p1.perimetre());
		System.out.println("Angle de rotation de la figure : " + p1.getRotation());
		
		
		System.out.println("\n\n---TEST DES HERITAGES---");
		//Triangles
		System.out.println("\n---Test de la classe TriangleRectangle---");
		System.out.println("new TriangleRectangle(1, 1, 0, 4, 3)");
		TriangleRectangle tr1 = new TriangleRectangle(1, 1, 0, 4, 3);
		System.out.println("Affichage de tr1 : " + tr1.toString());
		points = tr1.points();
		System.out.println("Liste des points du triangle :");
		for (int i = 0; i < 3; i++) System.out.println("Point " + (i+1) + " : " + points[i].toString());
		angles = tr1.angles();
		System.out.println("Liste des angles du triangle :");
		for (int i = 0; i < 3; i++) System.out.println("Angle " + (i+1) + " : " + angles[i]);
		System.out.println("Aire de tr1 : \t\t" + tr1.aire());
		System.out.println("Périmètre de tr1 : \t" + tr1.perimetre());
		System.out.println("Angle de rotation de la figure : " + tr1.getRotation());
		System.out.println("Le triangle est isocèle: " + tr1.estIsocele());
		System.out.println("Le triangle est rectangle : " + tr1.estTriRect());
		System.out.println("Le triangle est equilatéral : " + tr1.estEquilateral());
		
		System.out.println("\n---Test de la classe TriangleIsocele---");
		System.out.println("new TriangleIsocele(2, 3, 4.5, 50, 3.2)");
		TriangleIsocele ti1 = new TriangleIsocele(2, 3, 4.5, 50, 3.2);
		System.out.println("Affichage de ti1 : " + ti1.toString());
		points = ti1.points();
		System.out.println("Liste des points du triangle :");
		for (int i = 0; i < 3; i++) System.out.println("Point " + (i+1) + " : " + points[i].toString());
		angles = ti1.angles();
		System.out.println("Liste des angles du triangle :");
		for (int i = 0; i < 3; i++) System.out.println("Angle " + (i+1) + " : " + angles[i]);
		System.out.println("Aire de ti1 : \t\t" + ti1.aire());
		System.out.println("Périmètre de ti1 : \t" + ti1.perimetre());
		System.out.println("Angle de rotation de la figure : " + ti1.getRotation());
		System.out.println("Le triangle est isocèle: " + ti1.estIsocele());
		System.out.println("Le triangle est rectangle : " + ti1.estTriRect());
		System.out.println("Le triangle est equilatéral : " + ti1.estEquilateral());
		
		System.out.println("\n---Test de la classe TriangleEquilateral---");
		System.out.println("new TriangleEquilateral(2, 3, 4.5, 50, 3.2)");
		TriangleEquilateral te1 = new TriangleEquilateral(2, 3, 4.5, 50, 3.2);
		System.out.println("Affichage de te1 : " + te1.toString());
		points = te1.points();
		System.out.println("Liste des points du triangle :");
		for (int i = 0; i < 3; i++) System.out.println("Point " + (i+1) + " : " + points[i].toString());
		angles = te1.angles();
		System.out.println("Liste des angles du triangle :");
		for (int i = 0; i < 3; i++) System.out.println("Angle " + (i+1) + " : " + angles[i]);
		System.out.println("Aire de te1 : \t\t" + te1.aire());
		System.out.println("Périmètre de te1 : \t" + te1.perimetre());
		System.out.println("Angle de rotation de la figure : " + te1.getRotation());
		System.out.println("Le triangle est isocèle: " + ti1.estIsocele());
		System.out.println("Le triangle est rectangle : " + ti1.estTriRect());
		System.out.println("Le triangle est equilatéral : " + te1.estEquilateral());
		
		//Parallélogrammes
		System.out.println("\n---Test de la classe Losange---");
		System.out.println("new Losange(2, 3, 4.5, 50, 3.2)");
		Losange l1 = new Losange(2, 3, 4.5, 50, 3.2);
		System.out.println("Affichage de l1 : " + l1.toString());
		points = l1.points();
		System.out.println("Liste des points du losange :");
		for (int i = 0; i < 4; i++) System.out.println("Point " + (i+1) + " : " + points[i].toString());
		System.out.println("Aire de l1 : \t\t" + l1.aire());
		System.out.println("Périmètre de l1 : \t" + l1.perimetre());
		System.out.println("Angle de rotation de la figure : " + l1.getRotation());
		System.out.println("Le parallélogramme est un carré : " + l1.estCarre());
		System.out.println("Le parallélogramme est un losange : " + l1.estLosange());
		System.out.println("Le parallélogramme est un rectangle : " + l1.estRectangle());
		
		System.out.println("\n---Test de la classe Rectangle---");
		System.out.println("new Rectangle(4.5, 3, 2, 6.5, 3.5)");
		Rectangle r1 = new Rectangle(4.5, 3, 2, 6.5, 3.5);
		System.out.println("Affichage de r1 : " + r1.toString());
		points = r1.points();
		System.out.println("Liste des points du rectangle:");
		for (int i = 0; i < 4; i++) System.out.println("Point " + (i+1) + " : " + points[i].toString());
		System.out.println("Aire de r1 : \t\t" + r1.aire());
		System.out.println("Périmètre de r1 : \t" + r1.perimetre());
		System.out.println("Angle de rotation de la figure : " + r1.getRotation());
		System.out.println("Le parallélogramme est un carré : " + r1.estCarre());
		System.out.println("Le parallélogramme est un losange : " + r1.estLosange());
		System.out.println("Le parallélogramme est un rectangle : " + r1.estRectangle());
		
		System.out.println("\n---Test de la classe Carre---");
		System.out.println("new Carre(4.6, 2, 3.2, 9);");
		Carre c1 = new Carre(4.6, 2, 3.2, 9);
		System.out.println("Affichage de c1 : " + c1.toString());
		points = c1.points();
		System.out.println("Liste des points du carré :");
		for (int i = 0; i < 4; i++) System.out.println("Point " + (i+1) + " : " + points[i].toString());
		System.out.println("Aire de c1 : \t\t" + c1.aire());
		System.out.println("Périmètre de c1 : \t" + c1.perimetre());
		System.out.println("Angle de rotation de la figure : " + c1.getRotation());
		System.out.println("Le parallélogramme est un carré : " + c1.estCarre());
		System.out.println("Le parallélogramme est un losange : " + c1.estLosange());
		System.out.println("Le parallélogramme est un rectangle : " + c1.estRectangle());
		
		
	}
	
}
