
public class Disque extends Forme {

	private double r;
	
	Disque(double x, double y, double r) {
		super(x, y);
		this.r = r;
	}
	
	Disque(Point p, double r) {
		super(p);
		this.r = r;
	}
	
	public double aire() {
		return Math.PI * Math.pow(this.rayon(), 2.0);
	}
	
	public double perimetre() {
		return 2.0 * Math.PI * this.rayon();
	}
	
	public double diametre() {
		return 2.0 * this.rayon();
	}

	public boolean estDansDisque(Point p) {
		return Math.pow((p.getX() - this.p.getX()), 2.0) + Math.pow((p.getY() - this.p.getY()), 2.0) <= Math.pow(this.rayon(), 2.0);
	}
	
	public boolean estSurLeCercle(Point p) {
		return Math.pow((p.getX() - this.p.getX()), 2.0) + Math.pow((p.getY() - this.p.getY()), 2.0) == Math.pow(this.rayon(), 2.0);
	}
	
	public String toString() {
		return "centre:" + this.p + ";rayon=" + this.rayon() + ";";
	}
	
	//getter
	public double rayon() {
		return r;
	}
	
	
}
