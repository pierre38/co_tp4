
public class TriangleEquilateral extends TriangleIsocele {

	TriangleEquilateral(Point p, double alpha, double l) {
		super(p, alpha, l, Math.PI / 3.0);
	}
	
	TriangleEquilateral(double x, double y, double alpha, double l, double angle) {
		this(new Point(x, y), alpha, l);
	}
	
	public String toString() {
		return super.toString();
	}
	
}
